---
layout: markdown_page
title: "2018 Q2 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. 120% of plan, pipeline 3x minus in quarter, 100% at 70% of quota.

* CMO: Build 3x minus in quarter pipeline for Q3. % of plan achieved.
  * MSD: Generate sufficient demand to support our IACV targets. % of opportunity value creation target achieved.
    * BDR: Support efficient inbound demand creation. Achieve 110% of SAO plan.
    * Content: Publish content on the marketing site to accelrate inbound demand. Publish v2 of /customers. Publish /devops.
    * Content: Execute DevOps integrated campaign to support SDR and Field Marketing demand generation. Produce 2 webinars with 500 registrants. Distribute Gary Gruver's book to 600 people.
    * Field Marketing: Develop account based marketing strategies to deploy in support of Regional Director territory plans. Generate 41% of opportunity creation target worth of referral sourced opportunities.
    * Field Marketing: Execute on field event plan, pre, during, and post event. Generate 12% of opportunity creation target worth of opportunity sourced through field events.
    * Marketing Ops: Improve campaign tracking. Track UTM parameter values in salesforce.com for closed loop reporting in salesforce.com.
    * Marketing Ops: Improve trial experience and trial engagement. Launch new email nurture series educating trial requesters on EEU. Increase trial to opportunity conversion rate by 20%.
    * Online Growth: Extend SEO/PPC/Digital Advertising programs. Generate 31% of opportunity creation target worth of opportunity originating from the marketing site. Increase the amount of traffic to about.gitlab.com by 10% compared to last quarter.
    * Online Growth: Evaluate and build out ROIDNA CRO project. Increase GitLab EEU trial sign-ups by 15%. Increase GitLab EE downloads by 15%.
    * SCA: Support self serve SMB business. Achieve 130% of SMB IACV plan.
    * SDR: Generate outbound opportunity value. Source 16% of opportunity creation target worth of opportunity through outbound prospecting.
  * PMM: Complete messaging roll-out and activation to include: Sales, Partner and Marketing enablement, tier plans specific messaging and positioning, demo aligned to new positioning and messaging, presenting new messaging at key conferences.
  * PMM: Optimize trial sign-up, trial enablement and trial period experience, including the addition of GitLab.com trial and enhance trial nurture program.
  * PMM: Submit strong submission for Gartner Application Release Orchestration (ARO) MQ and contribute to SCM Market Guide update and continue briefing sweep with all key Gartner and Forrester analysts.
  * Outreach: Raise awareness. Double active evangelists. Launch education program. Double number of likes/upvotes/replies.
  * Outreach: Keep being an open source project. Increase active committers by 50%
  * Outreach: Get open source projects to use GitLab. Convert 3 large projects to self-hosted GitLab.
* CMO: Enough opportunities for strategic account leaders. Make the Q2 SCLAU forecast.
  * MSD: Achieve SCLAU volume target. Inbound SCLAU generation and outbound SCLAU generation.
    * SDR: Achieve SDR SCLAU volume targets. % of revised Q2 SDR targets.
    * Field Marketing: Achieve Field Marketing SCLAU volume targets. % of revised Q2 Field Marketing targets.
    * BDR: Achieve BDR SCLAU volume targets. % of revised Q2 BDR targets.
* CRO: 120% of plan achieved.
* CRO: Success Plans for all eligible customers.
  * Customer Success: Enabling a transition to Transformational Selling
    * Solutions Architects: Each Solutions Architect record video's of top 5 specialized use cases (including pitching services), reviewed by Customer Success leadership team.
    * Technical Account Managers: Do a quarterly business review for all eligible customers
    * Implementation Engineering: 75% of Big and Jumbo opportunities include Professional Services line item
  * Customer Success: 80% of opportunities advanced to stage 4 (Proposal) from stage 3 (Technical Evaluation) stage based on guided POC’s.
    * Solutions Architects: 100% of Solutions Architect team members participate in at least 1 guided POC.
    * Technical Account Managers: 100% of TAM team members participate in at least 1 guided POC.
    * Implementation Engineering: Create top 3 integration demonstration / test systems (LDAP, Jenkins, JIRA).
* CRO: Effective sales organization. 70% of salespeople are at 100% of quota.
  * Dir Channel: Increase Channel ASP by 50%
  * Dir Channel: Triple number of resellers above "Authorized Level"
  * Dir Channel: Implement VAR program (SHI, Insight, SoftwareOne, etc)
  * Sales Ops: Complete MEDDPIC sales methodology training. Account Executives and Account Managers should be proficient in capturing all MEDDPIC data points.
  * Sales Ops: Collaborate with Regional Directors to improve our conversion process in the earlier stages, more specifically between 1-Discovery and 2-Scoping as this is historically our lowest conversion.
  * Sales Ops: Complete 1:1 relationship between Accounts, Billing Accounts, and Subscription, where applicable. This will ensure a much cleaner CRM.
* CFO: Compliant operations. 3 projects completed.
  * Legal: GDPR policy fully implemented.
  * Legal: Contract management system for non-sales related contracts.
  * Billing Specialist: Add cash collection, application and compensation to job responsibilities.
* VPE
  * Director of Support
    * Support Engineering: 100% SLA achievement for premium self-hosted customers
    * Support Engineering: Document and implement severity-based ticket processing workflow for self-hosted customers
    * Support Services: 100% SLA achievement for GitLab.com customers
    * Support Services: Develop and document Support Services workflows, processes, and automation needed to deliver world-class customer support

### CEO: Popular next generation product. Ship first iteration of complete DevOps, GitLab.com uptime, zero click cluster demo.

* VP Product
  * Product: Ship first iteration of [complete DevOps](/2017/10/11/from-dev-to-devops/).
  * Product: Create a dashboard of usage of features. Replace Redash with Looker.
  * Product: Identify causes of free and paid churn on GitLab.com.
* CFO: Make progress on having public clouds run us. 2 running everything.
  * Dir. Partnerships: Sign agreement to migrate target OS project
  * Dir. Partnerships: Strategic cloud partner chooses GitLab SCM for an offering
  * Dir. Partnerships: Successfully track and present data on the usage touch points for attribution tracking of our cloud agreement
* CTO: Make sure cloud native installation, PaaS and cluster work well. Zero clicks.
* CTO: Make sure we [use our own features](https://gitlab.com/gitlab-org/gitlab-ce/issues/43807). Monitoring, CE review app, Auto DevOps for version and license.
* CTO: Jupyter integrated into GitLab. Hub deploy to cluster and Lab works with GitLab.
* VPE: Make GitLab.com ready for mission critical customer workloads (99.95% availability)
  * Eng Fellow: Improve monitoring by shipping 5 alerts that catch critical GitLab problems
  * UX: Deliver three UX Ready experience improvements per release towards reducing the installation time of DevOps.
  * UX: [Deliver three UX Ready experience improvements per release towards onboarding and authentication on gitlab.com.](https://gitlab.com/gitlab-org/ux-research/issues/54).
  * Quality: Deliver the first iteration of [engineering dashboard charts and metrics](https://gitlab.com/gitlab-org/gitlab-insights/issues/1).
  * Quality: Complete the organization of files, directories and LoC into /ee/ directory.
  * Security: Automated enforcement of GCP Security Guidelines
  * Security: Design, document, and implement security release process and craft epic with S1 & S2 issues and present to product for prioritization
  * Frontend: Deliver 100% of committed issues per release (10.8: 30/38 deliverables, 4/12 stretch; 11.0: x/y deliverables, x/y stretch; 11.1: x/y deliverables, x/y stretch)
  * Frontend: Integrate the first 3 reusable Vue components based on design.gitlab.com
  * Dev Backend: Define KPIs and build monitoring for release cycle performance
  * Dev Backend: Create first iteration of engineering management training materials and merge into handbook
    * Platform: Deliver 100% of committed issues per release (10.8: 15/21 deliverables, 1/14 stretch; 11.0: 12/17 deliverables, 3/13 stretch; 11.1: x/y deliverables, x/y stretch)
    * Platform: Ship first GraphQL endpoint to be used by an existing frontend component
    * Discussion: Deliver 100% of committed issues per release (10.8: 9/10 deliverables, 6/6 stretch; 11.0: 12/14 deliverables, 7/11 stretch; 11.1: x/y deliverables, x/y stretch)
    * Discussion: Make GitLab a Rails 5 app by default
    * Distribution: Deliver 100% of committed issues per release (10.8: 16/16 deliverables, 1/5 stretch; 11.0: 9/9 deliverables, 1/1 stretch; 11.1: x/y deliverables, x/y stretch)
    * Distribution: [Increase integration test coverage of HA setup](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/124)
    * Geo: Deliver 100% of committed issues per release (10.8: 10/20 deliverables, 3/8 stretch; 11.0: 8/10 deliverables, 2/2 stretch; 11.1: x/y deliverables, x/y stretch)
    * Geo: Test and perform multi-node secondary failover on GitLab.com to GCP
  * Ops Backend: Design and implement a hiring pool process for Ops backend (possibly in collaboration with Dev Backend)
  * Ops Backend: Goal #2
    * CI/CD: Deliver 100% of committed issues per release (10.8: 27/38 deliverables, 3/10 stretch; 11.0: x/y deliverables, x/y stretch; 11.1: x/y deliverables, x/y stretch)
    * CI/CD: Cover demo of Auto DevOps with GitLab QA
    * Monitoring: Deliver 100% of committed issues per release (10.8: 7/16 deliverables, 0/5 stretch; 11.0: x/y deliverables, x/y stretch; 11.1: x/y deliverables, x/y stretch)
    * Monitoring: [Publish official Grafana dashboards](https://gitlab.com/gitlab-org/gitlab-ce/issues/35062) (50% complete, first iteration merged)
    * Security Products: [Gemnasium infrastructure moved to GitLab](https://gitlab.com/groups/gitlab-org/-/epics/66)
  * Infrastructure
    * Production: Move to GCP in April
    * Database: [Improve application performance](https://gitlab.com/gitlab-com/database/issues/44)
    * Database: [Improve monitoring, configuration, and knowledge of our infrastructure](https://gitlab.com/gitlab-com/database/issues/43)
    * Database: [Improve team structure and workflow](https://gitlab.com/gitlab-com/infrastructure/issues/3917)
    * Gitaly: Ship v1.0 and turn off NFS
    * Gitaly: Get to v1.1 remove rugged code

### CEO: Great team. Active recruiting for all vacancies, number of diverse per vacancy, real-time dashboard.

* CCO: Active recruiting. 100% of vacancies have outbound sourcing.
* CCO: Increase double diverse candidates (underrepresented and low rent index). At least one qualified diverse candidate interviewed for each vacancy.
* CCO: Increase leadership aptitude and effectiveness for the executive team. At least one training per month, one leadership book per quarter, and improvement in 360 feedback.
* CFO: Real-time dashboard for everything in the Metrics sheet. 100% of metrics.
  * Legal: Scalable solution for hiring added in at least 5 countries.
  * Controller: Close cycle reduced to 9 days.
  * Controller: Audit fieldwork completed with no material weaknesses reported.
* VPE: Refactor engineering handbook to reflect org structure and have leaders take ownership of their sections
* VPE: Source 150 candidates and hire a director of infra, director of support, and a prod manager: Sourced 150 (100%), Hired 3 (100%)
  * Dev Backend: Source 50 candidates and hire a geo manager: Sourced 50 (100%), Hired 1 (100%)
    * Discussion: Source 150 candidates and hire 3 developers: Sourced 160 (100%), Hired 2 (66%)
    * Platform: Source 150 candidates and hire 3 developers: Sourced 160 (100%), Hired 1 (33%)
    * Distribution: Source 50 candidates and hire 1 engineer: Sourced 52 (110%), Hired 0 (0%)
  * Ops Backend: Source 50 candidates and hire 1 monitoring manager: Sourced 56 (112%), Hired X (X%)
    * Monitoring: Source 100 candidates and hire 2 monitoring engineers: Sourced 100 (100%), Hired X (X%)
    * CI/CD: Source 100 candidates and hire 2 engineers: Sourced 101 (101%), Hired X (X%)
    * Security Products: Source 50 candidates and hire 1 [Backend Developer](https://jobs.lever.co/gitlab/436d643c-1ff1-4115-8e4c-b1285ab72939): Sourced 50 (100%), Hired X (X%)
  * Quality: Source 100 candidates and hire 2 test automation Engineers: Sourced 128 (128%), Hired 0 (0%)
  * Frontend: Source 150 candidates and hire 3 developers: Sourced 10 - In progress (6%), Hired 3 (100%)
  * Infrastructure: Source 50 candidates and hire a database manager: Sourced 50 (100%), Hired X (0%)
    * Production: Source 200 candidates and hire 4 production engineers: Sourced 71 (35.5%), Hired 2 (50%)
    * Database: Source 100 candidates and hire 1 database engineers and 1 manager: Sourced 20 (20%), Hired 0 (0%)
    * Gitaly: Source 100 candidates and hire 2 Gitaly developers: Sourced 100 (100%), Hired 0 (0%)
  * UX: Source 50 candidates and hire a UX designer: Sourced 74 (148%), Hired 1 (100%)
  * Security: Source 150 candidates and hire an Anti-abuse Analyst, a SecOps Engineer, and a Compliance Analyst: Sourced 150 (100%), Hired 1 (33%)
  * Support Engineering: Source 210 candidates and hire 1 support engineering manager and 6 support engineers: Sourced X (X%), Hired X (X%)
    * Support Services: Source X candidates and hire X: Sourced X (X%), Hired X (X%)
* CMO: Hire to plan and prevent burnout. DevRel team plan, corporate marketing team plan, marketing and sales development team plan.
  * MSD: Hire to plan. SDR team plan, Field Marketing team plan, Online Growth team plan
