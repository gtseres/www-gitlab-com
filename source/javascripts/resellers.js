$(function() {
  var $regions = $('.reseller-region');
  var $regionItems = $('.reseller-item');
  var $resellerGroups = $('.reseller-group');

  $regions.on('click', function() {
    var $element = $(this);
    var id = $element.attr('id');

    if ($element.hasClass('active')) {
      $regions.removeClass('active');
      $resellerGroups.removeClass('reseller-group-filtered');
      if (window.innerWidth >= 993) {
        $('.reseller-item:not(:first-child)').css('margin-left', '50px');
      }
      if (window.innerWidth < 993 && window.innerWidth >= 768) {
        $('.reseller-item:not(:first-child)').css('margin-left', '20px');
      }
      if (window.innerWidth < 768) {
        $('.reseller-item:not(:first-child)').css('margin-left', '0');
      }
      $regionItems.removeClass('zoomIn')
        .fadeOut().promise().done(function() {
          $regionItems.addClass('zoomIn').fadeIn();
        });
    } else {
      $regions.removeClass('active');
      $resellerGroups.addClass('reseller-group-filtered');
      $('.reseller-item:not(:first-child)').css('margin-left', '0');
      $element.addClass('active');
      $regionItems.removeClass('zoomIn')
        .fadeOut(100).promise().done(function() {
          $regionItems.filter('.region-' + id).addClass('zoomIn').fadeIn(200);
        });
    }
  });
});
