---
layout: markdown_page
title: 2FA Removal
category: Support Workflows
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Use this when a request to disable 2FA on GitLab.com is received. 

## Notes: 

[Two-factor Authentication](http://docs.gitlab.com/ee/profile/two_factor_authentication.html) (2FA)
can only be removed from a GitLab.com account under the following circumstances:

### User has a valid SSH key:

Users can generate new recovery codes using SSH, if they've previously added
SSH public keys to their profile. The new recovery codes can then be used at
sign in. This option is presented to users in the Zendesk macro. If they cannot
use this method then move on to the manual methods below.

```
ssh git@gitlab.com 2fa_recovery_codes
```

### User has recovery codes
Users can try and login using your saved [two-factor recovery codes](http://docs.gitlab.com/ee/profile/two_factor_authentication.html#recovery-codes)

### User can provide evidence of account ownership:
Through:
- Government issued ID
- Repository verification

______________

##### Workflow

1. Apply the **"Account::2FA Removal Verification - GitLab.com"** Macro
2. Mark the ticket as "Pending"


##### If the user in unable to remove his 2FA from the above 2 methodas and User responds with ID verification 

1. Verify  if the originating email is the same as is on the account.

1. There should 2 images that are required.`Photo ID` and `picture of owner holding the Photo ID that is shared`.
2. Ensure the provided photo includes the persons face and ID. 

1. Ensure the provided ID is one of the following:
    + Driver's License
    + Passport
    + Military/Government ID
    + Permanent Resident Cards

1. Confirm the ID matches the users last and first name on the GitLab.com account.
2. Verify if that is **not** an expired ID. 
3. Only reset 2FA if the name on the account matches the name on the ID. If you can't read the ID, coordinate with a native language speaker
4. To disable, log into your admin account and locate the username in the users table or by going to 'https://gitlab.com/admin/users/usernamegoeshere'
5. Under the account tab, disable 2FA.


1. Provided the ID matches, use the **Account::2FA Removal Verification - GitLab.com - Successful** Macro
2. Mark the ticket as "Solved" 


##### User responds with repository verification 

1. Verify the file uploaded
    + File contains the provided text string.
    + File has been uploaded to a "Personal Repository"

2. Apply an "Internal Comment" with a link to the commit (if not already included)
3. Apply the ID matches, use the **Account::2FA Removal Verification - GitLab.com - Successful** Macro

##### Failed to verify provided ID or repository 

1. Apply the **Account::2FA Removal Verification - GitLab.com - Failed** Macro


##### User is unable to provide verification

If the user claims they're unable to provide identification we should review the matter internally (Support Team) and determine the best course of action. 

__________________

**Macros**

* [Account::2FA Removal Verification - GitLab.com](https://gitlab.zendesk.com/agent/admin/macros/103721068)
* [Account::2FA Removal Verification - GitLab.com - Failed](https://gitlab.zendesk.com/agent/admin/macros/103790308)
* [Account::2FA Removal Verification - GitLab.com - Successful](https://gitlab.zendesk.com/agent/admin/macros/103772548)
* 

## GitLab Team Members 2FA Removal

If the user is a GitLab employee, follow the below process:

1. Perform steps for SSH key and recovery codes, if possible.

2. Confirm authenticity of the request by contacting the employee via phone or video call.

