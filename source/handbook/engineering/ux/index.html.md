---
layout: markdown_page
title: "UX Team"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## UX at GitLab

The mission of the GitLab UX team is simple, to make a software product that is easy to use and enables everyone to contribute. We are building GitLab for a diverse, global community. It is essential to understand that community; it’s behaviors, needs, and motivations. We believe in iterative improvements and value aggressive change. We will get some things wrong and quickly strive to make them right. We have a long way to go, that is why we are taking big steps.

We have four guiding values that drive our UX work. GitLab should be professional and productive, human and quirky, minimal and efficient, and immediately recognizable. You can learn about these values in detail on [design.gitlab.com](https://design.gitlab.com/).

## Meet The UX Team

Our UX team is made up of designers and researchers from all around the world. Each of them brings their own unique cultures, experiences, and strengths to GitLab. You can learn about each team member by visiting our [team page](/team/) and filtering by "UX".

## UX Weekly Call

The UX team gets together once a week to share and discuss a variety of topics including workflow shortcuts or processes, designs for feedback, UX articles, and conferences. Attendance is optional but participation is encouraged. Each call is recorded and added to the archive.

## General UX Workflow

The UX team works alongside the community, Product Managers (PMs), Frontend engineers (FE), and Backend engineers (BE). PMs are responsible for kicking-off initiatives, taking action, and setting the direction of the product. PMs don't own the product, they gather feedback and give team members and the community the space to suggest and create. UX should help drive and inform that vision as soon in the process as possible. We do this by conducting research as well as facilitating discussion with community members, PM, FE, and BE.

### Product development timeline

The product development timeline is described in the [engineering workflow](/handbook/engineering/workflow/#product-development-timeline) section of the handbook. We will also explain it here.

At GitLab we follow a monthly release cycle, shipping on the 22nd of each month.  We plan and track these monthly releases using [milestones](https://docs.gitlab.com/ee/user/project/milestones/).  

There are specific deadlines that inform team workflows and prioritization. Suppose we are talking about milestone `m` that will be shipped in month `M` (on the 22nd). We have the following deadlines:

- By month `M-1, 1st`:
  - Release scope is established. In-scope issues marked with milestone `m`.
  - `m` issues are updated with docs starter blurb. Release post (WIP merge request) created with `m` issues and docs starter blurbs.
- On month `M-1, 8th` (or next business day): Kickoff call
- By month `M, 7th`: Completed `m` issues with docs have been merged into master. Un-started or unfinished `m` issues are de-scoped from `m`, with `m` being removed from them.
- On or around `M, 15th`: [team retrospectives](/handbook/engineering/management/team-retrospectives) should happen so they can inform the [public retrospective](/handbook/engineering/workflow/#retrospective)
- On month `M, 22nd`: Release shipped to production. Release post published.
- On month `M 22nd`: The next Release has been tentatively planned by Product and has been shared with engineering for discussion.

Our release timelines are overlapping. For example, when a release is shipped to production on the 22nd, the scope for the following release has already been established earlier in that same month. The following visualization illustrates that overlapping concept.

![rolling-planning-visualization.png](/handbook/engineering/ux/rolling-planning-visualization.png)

### Milestone Planning

Between the 22nd and 1st of each month, the UX Manager meets with PMs and other Engineering managers to discuss the priorities for the upcoming milestone. The purpose of these meetings is to ensure that all understand the requirements and to assess whether or not there is the capacity to complete all the issues proposed. The issues set for a particular milestone must be decided by the 1st of each month to give the engineering teams time to plan. There will be occasions where priorities shift, and changes must be made after the 1st. We should remain flexible and understanding of these situations while doing our best to make sure these exceptions do not become the rule.

### Milestone Kickoff

Before working on the next release, we have a company kickoff call to explain what we expect to ship in the next release. This call is led by the product team and highlights significant improvements and features.

The UX team has its kickoff as part of the [UX weekly call](/handbook/engineering/ux#ux-weekly-call) directly preceding or following kickoff on the 8th of the month. Using the planning sheet as our guide, each designer walks through the issues they will be working on for that milestone. The kickoff fosters transparency and collaboration across the whole team so it is important that all UXers attend.

### Working with Product Managers

Every UX Designer is aligned with a PM. The UXer is responsible for the same features their PM oversees. UXers work alongside PM at each stage of the process. Planning, discovery, implementation, and further iteration. The area a UXer is responsible for is part of their title, e.g. "UX Designer, Platform." You can see which area of the product each UX Designer is aligned with in the [team org chart](/team/chart/). 

UXers may also serve as a "backup" designer for other areas of the product. This area will be listed on the [team org chart](/team/chart/) under their title as an expertise, e.g. "Platform expert."

The UX Designer meets with PMs and other engineers every month to discuss scheduling and prioritization. Once the milestone has been set, the UXer assigns themselves to the issues in their area that need UX. UXers should immediately engage in and facilitate a conversation with the PM and engineers involved with the issue. Communicate early and often.

Understanding which PM is responsible for which area of the product is essential. An excellent resource for this is the [who to talk to for what](/handbook/product/#who-to-talk-to-for-what) and [product categories](/handbook/product/categories/) sections of the handbook. The [product section](/handbook/product/) of the handbook will give you a deep dive into how product at GitLab thinks and works.

### UX Designer Workflow

Once assigned to an issue, there is general workflow UX Designers follow. This workflow is detailed in the [UX Designer](/handbook/engineering/ux/ux-designer) section of the handbook.

### UX Research Workflow

There is a general workflow UX Researchers follow. This workflow is detailed in the [UX Research](/handbook/engineering/ux/ux-research) section of the handbook.

### How we use labels

GitLab uses labels to categorize, prioritize, and track work. The following is a breakdown of the labels most directly related to the UX workflow. An overview of all the workflow label types and uses can be found in the [contributing doc](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#workflow-labels).

* [**UX** label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX): indicates that UX work is required on this issue.
* [**design artifact** label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=design%20artifact): indicates that the final expected output for this issue is a UX/design solution. The implementation of the final solution will be developed in a subsequent milestone. The UX/design solution is due on the 7th of the month corresponding to the milestone where the 22nd is the release date. (For example, 10.6 was released on March 22nd, 2018. **design artifact** labeled issues with milestone 10.6 were due on March 7, 2018. [This follows the same due date as the feature freeze for engineering](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#feature-freeze-on-the-7th-for-the-release-on-the-22nd).) 
* [**UI polish** label](https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UI+polish): indicates the issue covers _only_ visual improvement(s) to the existing user interface
* [**UX debt** label](https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+debt): indicates that the issue covers user experience improvement(s) that weren't fully implemented or could be refined after implementation.
* [**Regression** label](https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=regression): indicates a bug introduced in the latest release that broke correct behavior (see the [contribution guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#regression-issues) for more info).
* [**UX ready** label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX%20ready): indicates that all UX work is completed and all feedback has been addressed.

### Milestone Retro

After each release, we have a company retrospective call in which we discuss what went well, what went wrong, and what we can improve for the next release.

To understand the specific challenges faced by the UX team, we hold an async UX retrospective after every milestone. This retro is done within the planning document. The goal is to evaluate what went well, what didn’t go well, and how we can improve.

### Epics and issues

We use epics and issues to organize, discuss, and execute our day-to-day work. Epics are used to define a larger vision and goal for a feature or area of the product. Issues within the epic drive the actions taken to achieve the desired results. In most cases, PM and the UX Manager are utilizing epics to plan and execute efforts over several milestones. 

### Community Contributions

We want to make it as easy as possible for GitLab users to become GitLab contributors. The UX team should offer support, guidance, and resources to any community member interested in contributing code and/or UX improvements.

Issues that are beneficial to our users, 'nice to have(s),' that we currently do not have the capacity for or want to give priority to are labeled as [Accepting Merge Requests](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=Accepting+Merge+Requests) so that the community can contribute. Community contributors can submit merge requests for any issue they want, but the [Accepting Merge Requests](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=Accepting+Merge+Requests) label has a special meaning. It points to
changes that:

* We already agreed on,
* Are well-defined,
* Are likely to get accepted by a maintainer.

We want to avoid a situation where a contributor picks an [Accepting Merge Requests](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=Accepting+Merge+Requests) issue, and then their merge request gets closed because we realize that it does not fit our vision, or we want to solve it differently. 

As a member of the UX team, make sure to alert the UX Manager if you are asked to work on one of the issues but don’t have the capacity. The UX manager is responsible for encouraging the community member to suggest a solution, share a design proposal, and keep the conversation going.

For full details on Accepting Merge Requests and contributing in general, see the [contributing doc](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#label-for-community-contributors-accepting-merge-requests).


## UX Resources

### GitLab design project

The [GitLab design project](https://gitlab.com/gitlab-org/gitlab-design/) is primarily used by the UX Design team to host design files and hand them off for implementation. For details, please visit the [README](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md) for the project.

### GitLab Design System

The GitLab Design System was developed to increase iteration speed and bring consistency to the UI through the creation of reusable and robust components. This system helps keep the application [DRY](http://programmer.97things.oreilly.com/wiki/index.php/Don't_Repeat_Yourself) and allows designers to focus their efforts on solving user needs, rather than recreating elements and reinventing solutions. It also empowers Product, engineering, and the community to use these defined patterns when proposing solutions. The Design System can be viewed at [design.gitlab.com](https://design.gitlab.com/). It is currently a work in progress.

The project and repository for design.gitlab.com can be found [here](https://gitlab.com/gitlab-org/design.gitlab.com).

### GitLab SVGs

Our [GitLab SVGs](https://gitlab.com/gitlab-org/gitlab-svgs) repository manages all SVG assets by creating an SVG sprite out of icons and optimizing SVG based illustrations.

All of our SVGs can be previewed using [this URL](http://gitlab-org.gitlab.io/gitlab-svgs/)

### UX research project

The UX research project contains all research undertaken by GitLab's UX researchers. [This project](https://gitlab.com/gitlab-org/ux-research) is used for the organization and tracking of UX research issues only. 

### UX design archive

The [UX design archive](/handbook/engineering/ux/design-archive) is a
collection of key design issues broken down by specific areas of GitLab. It is
not a comprehensive list. It is intended to shed insight into key UX design
decisions.

### UX research archive

The [UX research archive](/handbook/engineering/ux/research-archive) is a
collection of UX Research broken down by specific areas of GitLab. It is
intended to shed insight into key UX design decisions and the data that informed
those decisions.

## UX on social media

It is encouraged to share UX designs and insight on social media platforms such as Twitter and Dribbble.
 
### Twitter

You can contribute design-related posts to our [@GitLab Twitter account](https://twitter.com/gitlab) by adding your tweet to our [UX Design Twitter spreadsheet][twitter-sheet].

1. Add a new row with your tweet message, a relevant link, and an optional photo.
1. Ensure that your tweet is no more than 280 characters. If you’re including a link, ensure you have enough characters and consider using a [link shortening service](https://bitly.com/).
1. The UX Lead will check the spreadsheet at the beginning of each week and schedule any tweets on Tweetdeck.
1. Once a tweet is scheduled, the tweet will be moved to the "Scheduled" tab of the spreadsheet.

### Dribbble

GitLab has a [Dribbble team account](https://dribbble.com/GitLab) where you can add work in progress, coming soon, and recently released works.

* If a Dribbble post has a corresponding open issue, link to the issue so designers can contribute on GitLab.
* Add the Dribbble post to our [UX Design Twitter spreadsheet][twitter-sheet], along with a link to the corresponding open issue if applicable.
* If you’re not a member of the GitLab Dribbble team and would like to be, contact the UX Lead to grant you membership.


[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
